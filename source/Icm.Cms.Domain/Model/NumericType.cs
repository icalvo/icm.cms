namespace Icm.Cms.Domain.Model
{
    public class NumericType : IType
    {
        public string Name
        {
            get
            {
                return "Numeric";
            }
        }

        public bool ValueIsCompatible(object value)
        {
            return value is double;
        }
    }
}
using System;
using System.Collections.Generic;
using System.Linq;
using Icm.Cms.Domain.Queries;
using Icm.Cms.Domain.Repositories;

namespace Icm.Cms.Domain.Model
{
    public interface IEntityType : IType, IEntityRepository
    {
        IEntity New();

        IEnumerable<Field> Fields { get; }
        void AddField(Field field);
        void CheckDuplicateField(Field field);
    }
}
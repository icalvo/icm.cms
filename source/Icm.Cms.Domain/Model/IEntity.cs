namespace Icm.Cms.Domain.Model
{
    public interface IEntity
    {
        IEntityType Type { get; }
        string Id { get; }
        T GetValue<T>(string fieldName);
        object this[string fieldName] { get; set; }
    }
}
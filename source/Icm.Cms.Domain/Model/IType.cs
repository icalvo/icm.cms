namespace Icm.Cms.Domain.Model
{
    public interface IType
    {
        string Name { get; }

        bool ValueIsCompatible(object value);
    }
}
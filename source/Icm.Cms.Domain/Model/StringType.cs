namespace Icm.Cms.Domain.Model
{
    public class StringType : IType
    {
        public string Name
        {
            get
            {
                return "Text";
            }
        }

        public bool ValueIsCompatible(object value)
        {
            return value is string;
        }
    }
}
﻿using System;

namespace Icm.Cms.Domain.Model
{

    internal class FieldValue
    {
        private object value;

        internal FieldValue(Field field)
        {
            this.Field = field;
        }

        internal Field Field { get; private set; }

        internal object Value
        {
            get
            {
                return this.value;
            }
            set
            {
                if (this.Field.Type.ValueIsCompatible(value))
                {
                    this.value = value;
                }
                else
                {
                    throw new InvalidCastException();
                }
            }
        }
    }
}
﻿using System.Diagnostics;

namespace Icm.Cms.Domain.Model
{
    [DebuggerDisplay("{Name} : {Type} {Description}")]
    public class Field
    {
        public Field(string name)
            : this(new StringType(), name, null)
        {
        }

        public Field(IType type, string name)
            : this(type, name, null)
        {
        }

        public Field(string name, string description)
            : this(new StringType(), name, description)
        {
        }

        public Field(IType type, string name, string description)
        {
            this.Name = name;
            this.Description = description;
            this.Type = type;
        }

        public string Name { get; private set; }
        public string Description { get; private set; }
        public string Tooltip { get; private set; }
        public IType Type { get; private set; }
    }
}
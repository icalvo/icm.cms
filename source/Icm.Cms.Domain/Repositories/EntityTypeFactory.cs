using Icm.Cms.Domain.Model;

namespace Icm.Cms.Domain.Repositories
{
    public class EntityTypeFactory
    {
        private readonly IEntityTypeRepository entityTypeRepository;
        private readonly IEntityRepository entityRepository;

        public EntityTypeFactory(IEntityTypeRepository entityTypeRepository, IEntityRepository entityRepository)
        {
            this.entityTypeRepository = entityTypeRepository;
            this.entityRepository = entityRepository;
        }

        public IEntityType BuildEntityType(string name)
        {
            return new EntityType(name, this.entityTypeRepository, this.entityRepository);
        }

        public IEntityType BuildEntityType(string name, IEntityType parent)
        {
            return new EntityType(name, parent, this.entityTypeRepository, this.entityRepository);
        } 
    }
}
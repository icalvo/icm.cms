﻿using System;
using System.Collections.Generic;
using System.Linq;
using Icm.Cms.Domain.Model;
using Icm.Cms.Domain.Queries;

namespace Icm.Cms.Domain.Repositories
{
    public class MemoryEntityRepository : IEntityRepository
    {
        private readonly Dictionary<string, IEntity> store;

        public MemoryEntityRepository()
        {
            this.store = new Dictionary<string, IEntity>();
        }

        public IEntity GetById(string id)
        {
            IEntity result;
            if (!this.store.TryGetValue(id, out result))
            {
                throw new KeyNotFoundException();
            }

            return result;
        }

        public IEnumerable<IEntity> Get(Paging paging)
        {
            return this.store.OrderBy(kvp => kvp.Key).Select(kvp => kvp.Value).Skip(paging.PageNumber - 1);
        }

        public IEnumerable<IEntity> Get(Query myQuery, Paging paging)
        {
            throw new NotImplementedException();
        }

        public void Add(IEntity entity)
        {
            if (entity == null) throw new ArgumentNullException("entity");

            this.store.Add(entity.Id, entity);
        }

        public void Save()
        {
        }
    }
}
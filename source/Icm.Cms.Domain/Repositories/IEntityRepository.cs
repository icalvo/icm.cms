﻿using System.Collections.Generic;
using Icm.Cms.Domain.Model;
using Icm.Cms.Domain.Queries;

namespace Icm.Cms.Domain.Repositories
{
    public interface IEntityRepository
    {
        IEntity GetById(string id);


        IEnumerable<IEntity> Get(Paging paging);
        IEnumerable<IEntity> Get(Query myQuery, Paging paging);

        void Add(IEntity entity);
        void Save();
    }
}

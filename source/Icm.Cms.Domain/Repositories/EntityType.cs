using System;
using System.Collections.Generic;
using System.Linq;
using Icm.Cms.Domain.Model;
using Icm.Cms.Domain.Queries;

namespace Icm.Cms.Domain.Repositories
{
    internal class EntityType : IEntityType
    {
        private readonly IEntityTypeRepository repository;
        private readonly IEntityRepository entityRepository;
        private readonly List<Field> fields;
        private string name;

        public EntityType(string name, IEntityTypeRepository repository, IEntityRepository entityRepository)
        {
            this.repository = repository;
            this.entityRepository = entityRepository;
            if (name == null) throw new ArgumentNullException("name");

            this.CheckDuplicateEntityType(name);

            this.Name = name;
            this.fields = new List<Field>();

            this.repository.Add(this);
        }

        public EntityType(string name, IEntityType parent, IEntityTypeRepository entityTypeRepository, IEntityRepository entityRepository)
            : this(name, entityTypeRepository, entityRepository)
        {
            this.Parent = parent;
        }

        public IEntityType Parent { get; set; }

        public string Name
        {
            get
            {
                return this.name;
            }
            private set
            {
                this.CheckDuplicateEntityType(value);
                this.name = value;
            }
        }

        public bool ValueIsCompatible(object value)
        {
            Entity entity = value as Entity;

            if (entity == null)
            {
                return false;
            }

            return entity.Type.Name == this.Name;
        }

        public IEntity New()
        {
            return new Entity(this);
        }

        public IEnumerable<Field> Fields
        {
            get
            {
                if (this.Parent == null)
                {
                    return this.fields;
                }
                else
                {
                    return this.fields.Union(this.Parent.Fields);
                }
            }
        }

        public void AddField(Field field)
        {
            if (field == null) throw new ArgumentNullException("field");

            this.CheckDuplicateField(field);

            this.fields.Add(field);
        }

        public void CheckDuplicateField(Field field)
        {
            if (this.fields.Any(fld => fld.Name == field.Name))
            {
                throw new DuplicateFieldException();
            }

            if (this.Parent != null)
            {
                this.Parent.CheckDuplicateField(field);
            }
        }

        private void CheckDuplicateEntityType(string nameToCheck)
        {
            IEntityType existingType = this.repository.GetByName(nameToCheck);

            if (existingType != null)
            {
                throw new DuplicateEntityTypeException();
            }
        }

        public IEntity GetById(string id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IEntity> Get(Paging paging)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IEntity> Get(Query myQuery, Paging paging)
        {
            throw new NotImplementedException();
        }

        public void Add(IEntity entity)
        {
            throw new NotImplementedException();
        }

        public void Save()
        {
            throw new NotImplementedException();
        }
    }
}
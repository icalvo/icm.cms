using System.Collections.Generic;
using Icm.Cms.Domain.Model;
using Icm.Cms.Domain.Queries;

namespace Icm.Cms.Domain.Repositories
{
    public interface IEntityTypeRepository
    {
        IEntityType GetByName(string name);
        void Add(IEntityType entityType);
        IEnumerable<IEntity> GetByQuery(Query myQuery);
    }
}
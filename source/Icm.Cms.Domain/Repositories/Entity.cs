﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Icm.Cms.Domain.Model;

namespace Icm.Cms.Domain.Repositories
{
    [DebuggerDisplay("Entity {Id} : {Type}")]
    internal class Entity : IEntity
    {
        private readonly IList<FieldValue> values;

        public Entity(IEntityType type)
        {
            this.Type = type;
            this.values = new List<FieldValue>();
            this.Id = Guid.NewGuid().ToString();
        }

        public IEntityType Type { get; private set; }

        public string Id { get; internal set; }

        internal IEnumerable<FieldValue> Values
        {
            get { return this.values; }
        }

        public T GetValue<T>(string fieldName)
        {
            T result = (T)this[fieldName];

            return result;
        }

        public object this[string fieldName]
        {
            get
            {
                FieldValue fieldValue = this.Values.FirstOrDefault(fldValue => fldValue.Field.Name == fieldName);
                if (fieldValue == null)
                {
                    Field field = this.Type.Fields.FirstOrDefault(fld => fld.Name == fieldName);

                    if (field == null)
                    {
                        throw new Exception("The field " + fieldName + " does not exist in entities of type " + this.Type.Name);
                    }

                    return null;
                }

                return fieldValue.Value;
            }
            set
            {
                FieldValue fieldValue = this.Values.FirstOrDefault(fldValue => fldValue.Field.Name == fieldName);
                if (fieldValue == null)
                {
                    Field field = this.Type.Fields.FirstOrDefault(fld => fld.Name == fieldName);

                    if (field == null)
                    {
                        throw new Exception("The field " + fieldName + " does not exist in entities of type " + this.Type.Name);
                    }

                    fieldValue = new FieldValue(field);
                    this.values.Add(fieldValue);
                }

                fieldValue.Value = value;                
            }
        }
    }
}

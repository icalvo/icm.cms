﻿using System;

namespace Icm.Cms.Domain.Queries
{
    public class Sorting
    {
        
    }

    public class Paging
    {
        private int pageNumber;
        private int pageSize;

        public int PageNumber
        {
            get { return this.pageNumber; }
            private set
            {
                if (value < 1)
                {
                    throw new ArgumentOutOfRangeException();
                }

                this.pageNumber = value;
            }
        }

        public int PageSize
        {
            get { return this.pageSize; }
            private set
            {
                if (value < 1)
                {
                    throw new ArgumentOutOfRangeException();
                }

                if (value > 100)
                {
                    throw new ArgumentOutOfRangeException();
                }

                this.pageSize = value;
            }
        }

        public Paging() : this(1, 50)
        {
        }

        public Paging(int pageNumber, int pageSize)
        {


            this.PageNumber = pageNumber;
            this.PageSize = pageSize;
        }
    }
}
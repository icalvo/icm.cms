﻿using System;
using System.Linq;
using Icm.Cms.Domain.Model;
using Icm.Cms.Domain.Repositories;
using Xunit;

namespace Icm.Cms.Domain.Test
{
    public class EntityTypeTest
    {
        [Fact]
        public void Ctor_WhenNullName_ThrowsArgumentNullException()
        {
            EntityTypeFactory factory = new EntityTypeFactory(new FakeEntityTypeRepository(), new MemoryEntityRepository());
            Exception exception = Record.Exception(() => factory.BuildEntityType(null));

            Assert.IsType<ArgumentNullException>(exception);
        }

        [Fact]
        public void Ctor_WhenDuplicateName_ThrowsDuplicateEntityTypeException()
        {
            EntityTypeFactory factory = new EntityTypeFactory(new FakeEntityTypeRepository(), new MemoryEntityRepository());
            Fakes.EntityTypeRepository = new FakeEntityTypeRepository();
            factory.BuildEntityType("Project");
            Exception exception = Record.Exception(() => factory.BuildEntityType("Project"));

            Assert.IsType<DuplicateEntityTypeException>(exception);
        }

        [Fact]
        public void AddField_WhenNullField_ThrowsArgumentNullException()
        {
            EntityTypeFactory factory = new EntityTypeFactory(new FakeEntityTypeRepository(), new MemoryEntityRepository());
            IEntityType projectType = factory.BuildEntityType("Project");

            Exception exception = Record.Exception(() => projectType.AddField(null));

            Assert.IsType<ArgumentNullException>(exception);
        }

        [Fact]
        public void AddField_WhenDuplicateFieldName_ThrowsDuplicateFieldException()
        {
            EntityTypeFactory factory = new EntityTypeFactory(new FakeEntityTypeRepository(), new MemoryEntityRepository());
            IEntityType projectType = factory.BuildEntityType("Project");

            projectType.AddField(new Field("Code", "The unique project code"));
            projectType.AddField(new Field("Name", "The project name"));

            Exception exception = Record.Exception(() => projectType.AddField(new Field("Name")));

            Assert.IsType<DuplicateFieldException>(exception);
        }

        [Fact]
        public void AddField_WhenDuplicateFieldNameInAncestor_ThrowsDuplicateFieldException()
        {
            EntityTypeFactory factory = new EntityTypeFactory(new FakeEntityTypeRepository(), new MemoryEntityRepository());
            IEntityType projectType = factory.BuildEntityType("Project");

            projectType.AddField(new Field("Code", "The unique project code"));
            projectType.AddField(new Field("Name", "The project name"));

            IEntityType forecastProjectType = factory.BuildEntityType("ForecastProject", projectType);

            Exception exception = Record.Exception(() => forecastProjectType.AddField(new Field("Name")));

            Assert.IsType<DuplicateFieldException>(exception);
        }

        [Fact]
        public void AddField_Success1()
        {
            EntityTypeFactory factory = new EntityTypeFactory(new FakeEntityTypeRepository(), new MemoryEntityRepository());
            IEntityType projectType = factory.BuildEntityType("Project");

            Field projectCodeField = new Field("Code", "The unique project code");
            Field projectNameField = new Field("Name", "The project name");

            IEntityType forecastProjectType = factory.BuildEntityType("ForecastProject", projectType);
            Field projectForecastField = new Field("Forecast", "The project forecast");

            // Act
            projectType.AddField(projectCodeField);
            projectType.AddField(projectNameField);
            forecastProjectType.AddField(projectForecastField);

            // Assert
            Assert.Equal(3, forecastProjectType.Fields.Count());

            Assert.Contains(projectCodeField, forecastProjectType.Fields);
            Assert.Contains(projectNameField, forecastProjectType.Fields);
            Assert.Contains(projectForecastField, forecastProjectType.Fields);
        }
    }
}
using Icm.Cms.Domain.Model;
using Icm.Cms.Domain.Repositories;

namespace Icm.Cms.Domain.Test
{
    public static class Fakes
    {
        public static FakeEntityTypeRepository EntityTypeRepository;
        public static MemoryEntityRepository EntityRepository;

        public static EntityTypeFactory EntityTypeFactory()
        {
            return new EntityTypeFactory(EntityTypeRepository, EntityRepository);
        }
    }
}
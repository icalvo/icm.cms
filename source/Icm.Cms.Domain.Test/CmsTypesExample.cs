using System.Collections;
using Icm.Cms.Domain.Model;
using Icm.Cms.Domain.Repositories;

namespace Icm.Cms.Domain.Test
{
    public class CmsTypesExample
    {
        public CmsTypesExample()
        {
            EntityTypeFactory factory = new EntityTypeFactory(new FakeEntityTypeRepository(), new MemoryEntityRepository());
            this.ProjectType = factory.BuildEntityType("Project");

            this.ProjectType.AddField(new Field("Code", "The unique project code"));
            this.ProjectType.AddField(new Field("Name", "The project name"));

            this.ForecastProjectType = factory.BuildEntityType("ForecastProject", this.ProjectType);
            this.ForecastProjectType.AddField(new Field(new NumericType(), "Forecast", "The project forecast"));

            this.RegionType = factory.BuildEntityType("Region");
            this.RegionType.AddField(new Field("Name", "The region name"));

            this.ProjectType.AddField(new Field(this.RegionType, "Region", "The region of the project"));
        }

        public IEntityType ForecastProjectType { get; private set; }

        public IEntityType ProjectType { get; private set; }

        public IEntityType RegionType { get; private set; }
    }
}
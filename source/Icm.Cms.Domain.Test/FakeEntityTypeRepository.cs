using System;
using System.Collections.Generic;
using System.Linq;
using Icm.Cms.Domain.Model;
using Icm.Cms.Domain.Queries;
using Icm.Cms.Domain.Repositories;

namespace Icm.Cms.Domain.Test
{
    public class FakeEntityTypeRepository : IEntityTypeRepository
    {
        private readonly IList<IEntityType> store;

        public FakeEntityTypeRepository(params IEntityType[] initialEntityTypes)
        {
            if (initialEntityTypes == null) throw new ArgumentNullException("initialEntityTypes");

            this.store = new List<IEntityType>(initialEntityTypes);
        }

        public IList<IEntityType> Store
        {
            get { return store; }
        }

        public IEntityType GetByName(string name)
        {
            return this.store.SingleOrDefault(type => type.Name == name);
        }

        public void Add(IEntityType entityType)
        {
            this.store.Add(entityType);
        }

        public IEnumerable<IEntity> GetByQuery(Query myQuery)
        {
            throw new NotImplementedException();
        }
    }
}
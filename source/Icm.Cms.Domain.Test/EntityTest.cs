﻿using System;
using System.Collections.Generic;
using System.Linq;
using Icm.Cms.Domain.Model;
using Icm.Cms.Domain.Queries;
using Icm.Cms.Domain.Repositories;
using Xunit;

namespace Icm.Cms.Domain.Test
{
    public class EntityTest
    {
        [Fact]
        public void Accessor_InvalidType_ThrowsInvalidCastException()
        {
            // ARRANGE
            // Set up CMS types
            EntityTypeFactory factory = new EntityTypeFactory(new FakeEntityTypeRepository(), new MemoryEntityRepository());
            IEntityType projectType = factory.BuildEntityType("Project");

            projectType.AddField(new Field("Code", "The unique project code"));
            projectType.AddField(new Field("Name", "The project name"));

            IEntityType regionType = factory.BuildEntityType("Region");
            regionType.AddField(new Field("Name", "The region name"));

            projectType.AddField(new Field(regionType, "Region", "The region of the project"));

            // Create entities
            IEntity project1 = projectType.New();

            // ACT
            Exception exception = Record.Exception(() => project1["Code"] = 345);

            // ASSERT
            Assert.IsType<InvalidCastException>(exception);
        }

        [Fact]
        public void Accessor_InvalidEntity_ThrowsInvalidCastException()
        {
            // ARRANGE
            // Set up CMS types
            EntityTypeFactory factory = new EntityTypeFactory(new FakeEntityTypeRepository(), new MemoryEntityRepository());
            IEntityType projectType = factory.BuildEntityType("Project");

            projectType.AddField(new Field("Code", "The unique project code"));
            projectType.AddField(new Field("Name", "The project name"));

            IEntityType regionType = factory.BuildEntityType("Region");
            regionType.AddField(new Field("Name", "The region name"));

            projectType.AddField(new Field(regionType, "Region", "The region of the project"));

            // Create entities
            IEntity project1 = projectType.New();

            // ACT
            Exception exception = Record.Exception(() => project1["Region"] = project1);

            // ASSERT
            Assert.IsType<InvalidCastException>(exception);
        }

        [Fact]
        public void Accessor_Success()
        {
            // ARRANGE
            CmsTypesExample types = new CmsTypesExample();


            // Create entities
            IEntity project1 = types.ProjectType.New();
            project1["Code"] = "123456";
            project1["Name"] = "Battlefield 4";

            IEntity project2 = types.ProjectType.New();
            project2["Code"] = "666666";
            project2["Name"] = "GTA 6";

            IEntity regionEurope = types.RegionType.New();
            regionEurope["Name"] = "Europe";

            project1["Region"] = regionEurope;

            IEntity forecastProject = types.ForecastProjectType.New();
            forecastProject["Code"] = "666666";
            forecastProject["Name"] = "GTA 6";
            forecastProject["Forecast"] = 345.25;
        }
    }

    public class MemoryRepositoryTests
    {
        [Fact]
        public void Accessor_Success()
        {
            // ARRANGE
            CmsTypesExample types = new CmsTypesExample();


            // Create entities
            IEntity project1 = types.ProjectType.New();
            project1["Code"] = "123456";
            project1["Name"] = "Battlefield 4";

            IEntity project2 = types.ProjectType.New();
            project2["Code"] = "666666";
            project2["Name"] = "GTA 6";

            IEntity regionEurope = types.RegionType.New();
            regionEurope["Name"] = "Europe";

            project1["Region"] = regionEurope;

            IEntityRepository entityRepository = new MemoryEntityRepository();

            entityRepository.Add(project1);
            entityRepository.Add(project2);
            entityRepository.Add(regionEurope);
        }        
    }
}